import { useEffect } from 'react';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

import logo from '@/assets/images/logo.png';
import { Button } from '@/components/Button/Button.styled';
import { PasswordField, TextField } from '@/components/Input';
import { useRegisterUserMutation } from '@/store/features/auth/authApi';
import { IRegister } from '@/types/user.types';

import {
  ButtonContainer,
  RegisterContainer,
  RegisterForm,
  RegisterImg,
} from './Register.styled';

const Register = () => {
  const { control, handleSubmit, watch } = useForm<IRegister>({
    defaultValues: {
      email: '',
      password: '',
      passwordConfirm: '',
    },
    mode: 'onChange',
  });

  const [registerUser, { isSuccess }] = useRegisterUserMutation();
  const nav = useNavigate();
  const onSubmit: SubmitHandler<IRegister> = async (data) => {
    await registerUser(data);
  };

  useEffect(() => {
    isSuccess ? nav('/login') : '';
  }, [isSuccess]);
  return (
    <RegisterForm onSubmit={handleSubmit(onSubmit)}>
      <RegisterImg draggable={false} src={logo} />
      <RegisterContainer>
        <Controller
          name="username"
          control={control}
          rules={{
            required: 'Логин обязателен',
          }}
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <TextField
              placeholder="Логин"
              error={error}
              onChange={onChange}
              value={value}
            />
          )}
        />
        <Controller
          name="email"
          control={control}
          rules={{
            required: 'E-mail обязателен',
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,}$/i,
              message: 'Укажите E-Mail',
            },
          }}
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <TextField
              placeholder="Почта"
              error={error}
              onChange={onChange}
              value={value}
            />
          )}
        />
        <Controller
          name="password"
          control={control}
          rules={{
            validate: {
              required: (v) => {
                if (!v.length) {
                  return 'Пароль обязателен';
                }
              },
              latin: (v) => {
                const regexp = /[A-Za-z]/;
                const match = regexp.exec(v);

                if (!match) {
                  return 'Пароль должен содержать латинские символы';
                }
              },
              length: (v) => {
                if (v.length < 8) {
                  return 'Пароль должен содержать больше 8 символов';
                }
              },
              oneCapital: (v) => {
                const regexp = /[A-Z]{1}/;
                const match = regexp.exec(v);

                if (!match) {
                  return 'Пароль должен содержать минимум 1 заглавную букву';
                }
              },
              oneDigit: (v) => {
                const regexp = /[0-9]{1}/;
                const match = regexp.exec(v);

                if (!match) {
                  return 'Пароль должен содержать минимум 1 цифру';
                }
              },
            },
          }}
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <PasswordField
              placeholder="Пароль"
              error={error}
              onChange={onChange}
              value={value}
            />
          )}
        />

        <Controller
          name="passwordConfirm"
          control={control}
          rules={{
            validate: {
              confirm: (v) => {
                if (watch('password') !== v) {
                  return 'Пароли должны совпадать';
                }
              },
            },
          }}
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <PasswordField
              placeholder="Повторите пароль"
              error={error}
              onChange={onChange}
              value={value}
            />
          )}
        />
      </RegisterContainer>
      <ButtonContainer>
        <Button $primary>Зарегистрироваться</Button>
      </ButtonContainer>
    </RegisterForm>
  );
};
export default Register;
