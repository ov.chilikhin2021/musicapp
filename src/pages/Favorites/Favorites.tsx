import { Heading } from '@/components/Heading/Heading.styled';
import PlayList from '@/components/PlayList/PlayList';
import { useGetFavoriteTracksQuery } from '@/store/features/track/trackApi';

const Favorites = () => {
  const { data, isLoading } = useGetFavoriteTracksQuery();

  return (
    <>
      <Heading>Мои треки</Heading>
      <PlayList data={data} isLoading={isLoading} />
    </>
  );
};
export default Favorites;
