import styled from 'styled-components';

export const Container = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  text-align: center;
  gap: 15px;
`;

export const Heading = styled.h1`
  display: flex;
  flex-direction: column;
  font-style: normal;
  font-weight: 400;
  font-size: 32px;
  line-height: 40px;
  position: relative;

  span {
    display: block;
    font-size: 160px;
    line-height: 168px;
  }
  img {
    bottom: 0;
    right: -60px;
    margin-left: 8px;
    position: absolute;
  }
`;

export const Subheading = styled.p`
  color: ${(props) => props.theme.gray};
  width: 284px;
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 24px;
`;
