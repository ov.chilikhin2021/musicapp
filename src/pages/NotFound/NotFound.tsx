import { useNavigate } from 'react-router-dom';

import img from '@/assets/images/smile_crying.png';
import { Button } from '@/components/Button/Button.styled';

import { Container, Heading, Subheading } from './NotFound.styled';

const NotFound = () => {
  const nav = useNavigate();
  return (
    <Container>
      <Heading>
        <span>404</span>
        Страница не найдена
        <img draggable={false} src={img} alt="cry" />
      </Heading>

      <Subheading>Возможно, она была удалена или перенесена на другой адрес</Subheading>

      <Button onClick={() => nav('/')} $primary>
        Вернуться на главную
      </Button>
    </Container>
  );
};
export default NotFound;
