import { useNavigate, useParams } from 'react-router-dom';

import { Heading } from '@/components/Heading/Heading.styled';
import PlayList from '@/components/PlayList/PlayList';
import { useGetSelectionByIdQuery } from '@/store/features/track/trackApi';

const Favorites = () => {
  const { id } = useParams();
  const { data, isLoading, isError } = useGetSelectionByIdQuery(Number(id));
  const nav = useNavigate();

  return (
    <>
      {!isError ? (
        <>
          <Heading>{data?.name}</Heading>
          <PlayList data={data?.items} isLoading={isLoading} />
        </>
      ) : (
        nav('/home')
      )}
    </>
  );
};
export default Favorites;
