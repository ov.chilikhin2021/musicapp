import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Filters from '@/components/Filters/Filters';
import { Heading } from '@/components/Heading/Heading.styled';
import PlayList from '@/components/PlayList/PlayList';
import Selection from '@/components/Selection/Selection';
import { setFilterData, setTracks } from '@/store/features/filter/filterSlice';
import { useGetAllTracksQuery } from '@/store/features/track/trackApi';
import { RootState } from '@/store/store';

import { Main } from './Home.styled';

const Home = () => {
  const { data, isLoading } = useGetAllTracksQuery();

  const { filterTracks, sortedByAsc, selectedGenres, selectedAuthors } = useSelector(
    (state: RootState) => state.filter,
  );

  const dispatch = useDispatch();

  useEffect(() => {
    data && dispatch(setTracks(data));
    dispatch(setFilterData());
  }, [data]);

  useEffect(() => {
    dispatch(setFilterData());
  }, [sortedByAsc, selectedGenres, selectedAuthors]);
  return (
    <>
      <Heading>Треки</Heading>
      <Filters />
      <Main>
        <PlayList data={filterTracks} isLoading={isLoading} />
        <Selection isLoading={isLoading} />
      </Main>
    </>
  );
};

export default Home;
