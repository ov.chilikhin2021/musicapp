import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import logo from '@/assets/images/logo.png';
import { Button } from '@/components/Button/Button.styled';
import { PasswordField, TextField } from '@/components/Input';
import {
  useLoginUserMutation,
  useTokenUserMutation,
} from '@/store/features/auth/authApi';
import { setTokens, setUser } from '@/store/features/user/userSlice';
import { Login as ILogin } from '@/types/user.types';

import { ButtonContainer, LoginContainer, LoginForm, LoginImg } from './Login.styled';

const Login = () => {
  const nav = useNavigate();
  const dispatch = useDispatch();
  const [tokenUser] = useTokenUserMutation();
  const [loginUser] = useLoginUserMutation();
  const { control, handleSubmit } = useForm<ILogin>({
    defaultValues: {
      email: '',
      password: '',
    },
    mode: 'onChange',
  });

  const onSubmit: SubmitHandler<ILogin> = async (data) => {
    const loginData = await loginUser(data).unwrap();
    const tokenData = await tokenUser(data).unwrap();

    dispatch(setUser(loginData));
    dispatch(setTokens(tokenData));
  };
  return (
    <LoginForm onSubmit={handleSubmit(onSubmit)}>
      <LoginImg draggable={false} src={logo} />
      <LoginContainer>
        <Controller
          name="email"
          control={control}
          rules={{
            required: 'E-mail обязателен',
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,}$/i,
              message: 'Укажите E-Mail',
            },
          }}
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <TextField
              placeholder="Почта"
              error={error}
              onChange={onChange}
              value={value}
            />
          )}
        />
        <Controller
          name="password"
          control={control}
          rules={{
            validate: {
              required: (v) => {
                if (!v.length) {
                  return 'Пароль обязателен';
                }
              },
              latin: (v) => {
                const regexp = /[A-Za-z]/;
                const match = regexp.exec(v);

                if (!match) {
                  return 'Пароль должен содержать латинские символы';
                }
              },
              length: (v) => {
                if (v.length < 8) {
                  return 'Пароль должен содержать больше 8 символов';
                }
              },
              oneCapital: (v) => {
                const regexp = /[A-Z]{1}/;
                const match = regexp.exec(v);

                if (!match) {
                  return 'Пароль должен содержать минимум 1 заглавную букву';
                }
              },
              oneDigit: (v) => {
                const regexp = /[0-9]{1}/;
                const match = regexp.exec(v);

                if (!match) {
                  return 'Пароль должен содержать минимум 1 цифру';
                }
              },
            },
          }}
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <PasswordField
              placeholder="Пароль"
              error={error}
              onChange={onChange}
              value={value}
            />
          )}
        />
      </LoginContainer>
      <ButtonContainer>
        <Button $primary>Войти</Button>
        <Button
          onClick={() => {
            nav('/register');
          }}
        >
          Зарегистрироваться
        </Button>
      </ButtonContainer>
    </LoginForm>
  );
};
export default Login;
