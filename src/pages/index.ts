import Collection from './Collection/Collection.';
import Favorites from './Favorites/Favorites';
import Home from './Home/Home';
import Login from './Login/Login';
import NotFound from './NotFound/NotFound';
import Register from './Register/Register';

export { Collection, Favorites, Home, Login, NotFound, Register };
