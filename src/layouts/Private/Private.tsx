import { FC, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Outlet, useNavigate } from 'react-router-dom';

import Center from '@/components/Header/Center/Center';
import Left from '@/components/Header/Left/Left';
import Right from '@/components/Header/Right/Right';
import Player from '@/components/Player/Player';
import { RootState } from '@/store/store';

import { Container, Header, Main, PrivateContainer } from './Private.styled';

type PrivateProps = {
  isAuth: boolean;
};
const Private: FC<PrivateProps> = ({ isAuth }) => {
  const currentSong = useSelector((state: RootState) => state.player.currentSong);

  const nav = useNavigate();

  useEffect(() => {
    isAuth ? nav('/home') : nav('/login');
  }, []);

  useEffect(() => {
    if (!isAuth) nav('/login');
  }, [isAuth]);

  return (
    <PrivateContainer>
      <Container>
        <Header>
          <Left />
          <Center />
          <Right />
        </Header>
        <Main>
          <Outlet />
        </Main>
      </Container>
      {currentSong ? <Player /> : ''}
    </PrivateContainer>
  );
};
export default Private;
