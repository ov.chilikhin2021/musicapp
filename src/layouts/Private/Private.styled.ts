import { styled } from 'styled-components';

export const PrivateContainer = styled.section`
  min-height: 100%;
  background-color: ${(props) => props.theme.backgroundColor};
  color: ${(props) => props.theme.color};
  display: flex;
  flex-direction: column;
`;

export const Container = styled.section`
  padding: 0 92px 0 36px;
  flex: 1;
  display: flex;
  flex-direction: column;
`;

export const Header = styled.header`
  padding: 36px 0;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Main = styled.main`
  margin-top: 15px;
  margin-left: 294px;

  display: flex;
  flex-direction: column;
  gap: 3rem;
`;
