import styled from 'styled-components';

export const Container = styled.section`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${(props) => props.theme.color};
  background: ${(props) => props.theme.backgroundColor};
`;
