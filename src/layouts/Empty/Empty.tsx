import { FC, useEffect } from 'react';
import { Outlet, useNavigate } from 'react-router-dom';

import { Container } from './Empty.styled';

type EmptyProps = {
  isAuth: boolean;
};

const Empty: FC<EmptyProps> = ({ isAuth }) => {
  const nav = useNavigate();

  useEffect(() => {
    isAuth ? nav('/home') : nav('/login');
  }, []);

  useEffect(() => {
    if (isAuth) nav('/home');
  }, [isAuth]);
  return (
    <Container>
      <Outlet />
    </Container>
  );
};
export default Empty;
