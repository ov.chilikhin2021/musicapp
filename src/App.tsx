import Global from 'App.styled';
import { Provider } from 'react-redux';

import { ThemeContext } from '@/context/ThemeContext';
import { store } from '@/store/store';

import { Router } from './routers/router';

const App = () => {
  return (
    <>
      <Provider store={store}>
        <ThemeContext>
          <Global />
          <Router />
        </ThemeContext>
      </Provider>
    </>
  );
};

export default App;
