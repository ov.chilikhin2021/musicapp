import { TbLogout } from 'react-icons/tb';
import { useDispatch } from 'react-redux';

import { logOut } from '@/store/features/user/userSlice';

import { Icon } from './Right.styled';

const Right = () => {
  const dispatch = useDispatch();
  return (
    <Icon
      onClick={() => {
        dispatch(logOut());
      }}
    >
      <TbLogout />
    </Icon>
  );
};
export default Right;
