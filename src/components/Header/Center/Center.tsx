import { ChangeEvent, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import { setSearch } from '@/store/features/filter/filterSlice';

import { Search, SearchIcon, Section } from './Center.styled';

const Center = () => {
  const [search, setSearchString] = useState('');
  const dispatch = useDispatch();

  useEffect(() => {
    const timerId = setTimeout(() => dispatch(setSearch(search)), 500);

    return () => {
      clearTimeout(timerId);
    };
  }, [search]);

  return (
    <Section>
      <SearchIcon />
      <Search
        onChange={(e: ChangeEvent<HTMLInputElement>) => {
          setSearchString(e.target.value);
        }}
        value={search}
        placeholder="Поиск"
      />
    </Section>
  );
};
export default Center;
