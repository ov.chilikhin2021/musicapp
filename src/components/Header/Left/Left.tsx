import { useState } from 'react';
import { TfiMenu } from 'react-icons/tfi';
import { Link } from 'react-router-dom';

import logo__dark from '@/assets/images/logo.png';
import logo from '@/assets/images/logo__white.png';
import SideBar from '@/components/SideBar/SideBar';
import { useThemeContext } from '@/context/ThemeContext';

import { Logo, Menu, Section } from './Left.styled';

const Left = () => {
  const [isActive, setIsActive] = useState<boolean>(false);

  const { theme } = useThemeContext();

  return (
    <Section>
      <SideBar isActive={isActive} setIsActive={setIsActive} />
      <Menu>
        <TfiMenu
          onClick={() => {
            setIsActive((prev) => !prev);
          }}
        />
      </Menu>
      <Link to="/home">
        <Logo src={theme === 'dark' ? logo : logo__dark} />
      </Link>
    </Section>
  );
};
export default Left;
