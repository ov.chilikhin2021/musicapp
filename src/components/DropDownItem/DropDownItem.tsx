import { FC, useId } from 'react';
import { useDispatch } from 'react-redux';

import {
  setSelectedAuthors,
  setSelectedGenres,
  setSorted,
} from '@/store/features/filter/filterSlice';
import { Author, Genre, Year } from '@/types/tracks.types';

import { CheckBox, Item, Label } from './DropDownItem.styled';

type DropDownItemType = {
  type: string;
  item: Author | Genre | Year;
};

const DropDownItem: FC<DropDownItemType> = ({ type, item }) => {
  const id = useId();

  const dispatch = useDispatch();

  const onClick = (type: string, item: Author | Genre | Year) => {
    switch (type) {
      case 'genre': {
        dispatch(setSelectedGenres(item));
        break;
      }

      case 'author': {
        dispatch(setSelectedAuthors(item));
        break;
      }

      default: {
        dispatch(setSorted(item));
        break;
      }
    }
  };

  if (type !== 'year') {
    return (
      <Item>
        <CheckBox type="checkbox" id={id} value={item.value} />
        <Label
          onClick={() => {
            onClick(type, item);
          }}
          htmlFor={id}
        >
          {item.name}
        </Label>
      </Item>
    );
  }

  return (
    <Item>
      <CheckBox name={type} type="radio" id={id} value={item.value} />
      <Label
        onClick={() => {
          onClick(type, item);
        }}
        htmlFor={id}
      >
        {item.name}
      </Label>
    </Item>
  );
};
export default DropDownItem;
