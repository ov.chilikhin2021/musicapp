import styled from 'styled-components';

export const Item = styled.li`
  font-size: 20px;
  line-height: 24px;
  font-weight: 400;
  transition: color 0.3s ease;

  &:hover {
    color: #b672ff;
  }
`;
export const Label = styled.label`
  cursor: pointer;
`;
export const CheckBox = styled.input`
  display: none;

  &:checked {
    + label {
      border-bottom: 1px solid #b672ff;
      color: #b672ff;
    }
  }
`;
