import { timer } from 'helpers/helper';
import { FC } from 'react';
import { TbMusic } from 'react-icons/tb';

import { useAuth } from '@/hooks/useAuth';
import {
  useAddFavoriteTrackMutation,
  useRemoveFavoriteTrackMutation,
} from '@/store/features/track/trackApi';
import { Track } from '@/types/tracks.types';

import {
  Column,
  ColumnSecondary,
  IconContainer,
  IconHeart,
  IconHeartOff,
  IconPlay,
  Item,
} from './PlayListItem.styled';

type Props = {
  data: Track;
  onClick: () => void;
};

const PlayListItem: FC<Props> = ({ data, onClick }) => {
  const [addFavoriteTrack] = useAddFavoriteTrackMutation();

  const [removeFavoriteTrack] = useRemoveFavoriteTrackMutation();

  const { user } = useAuth();

  const isFavorites =
    !data.stared_user || data.stared_user.filter((item) => item.id === user.id)[0];

  return (
    <Item>
      <Column>
        <IconContainer onClick={onClick}>
          <TbMusic />
          <IconPlay />
        </IconContainer>
        {data.name}
      </Column>
      <Column>{data.author}</Column>
      <ColumnSecondary>{data.album}</ColumnSecondary>
      <ColumnSecondary>
        {isFavorites ? (
          <IconHeartOff
            onClick={() => {
              removeFavoriteTrack(data.id);
            }}
          />
        ) : (
          <IconHeart
            onClick={() => {
              addFavoriteTrack(data.id);
            }}
          />
        )}

        {timer(data.duration_in_seconds)}
      </ColumnSecondary>
    </Item>
  );
};
export default PlayListItem;
