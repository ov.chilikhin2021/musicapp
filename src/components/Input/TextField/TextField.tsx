import { FC } from 'react';
import { FieldError } from 'react-hook-form';

import { Error, Input, InputContainer } from '../Input.styled';

type TextProps = {
  placeholder: string;
  error?: FieldError;
  onChange: () => void;
  value: string;
};

const TextField: FC<TextProps> = ({ placeholder, error, onChange, value }) => {
  return (
    <InputContainer>
      <Input
        placeholder={placeholder}
        className={error ? 'error' : ''}
        onChange={onChange}
        value={value}
      />
      {error && <Error>{error.message}</Error>}
    </InputContainer>
  );
};
export default TextField;
