import { FC, useState } from 'react';
import { FieldError } from 'react-hook-form';
import { MdOutlineVisibility, MdOutlineVisibilityOff } from 'react-icons/md';

import { Error, Input, InputContainer } from '../Input.styled';
import { IconContainer } from './Password.styled';

type PasswordProps = {
  placeholder: string;
  error?: FieldError;
  onChange: () => void;
  value: string;
};

const PasswordField: FC<PasswordProps> = ({ placeholder, error, onChange, value }) => {
  const [isVisible, setIsVisible] = useState<boolean>(false);

  return (
    <InputContainer>
      <Input
        type={isVisible ? 'text' : 'password'}
        placeholder={placeholder}
        onChange={onChange}
        className={error ? 'error' : ''}
        value={value}
      />
      <IconContainer
        onClick={() => {
          setIsVisible((prev) => !prev);
        }}
      >
        {isVisible ? <MdOutlineVisibilityOff /> : <MdOutlineVisibility />}
      </IconContainer>

      {error && <Error>{error.message}</Error>}
    </InputContainer>
  );
};
export default PasswordField;
