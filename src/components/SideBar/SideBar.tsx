import { Dispatch, FC, useRef } from 'react';
import { HiMoon, HiSun } from 'react-icons/hi2';
import { TfiClose } from 'react-icons/tfi';

import { useThemeContext } from '@/context/ThemeContext';
import useOnClickOutside from '@/hooks/useOnClickOutside';

import { Icon, Link, Nav, NavContainer, Overlay, Theme } from './SideBar.styled';

type SideBarProps = {
  isActive: boolean;
  setIsActive: Dispatch<React.SetStateAction<boolean>>;
};

const SideBar: FC<SideBarProps> = ({ isActive, setIsActive }) => {
  const sideBarRef = useRef<HTMLDivElement>(null);

  const clickOutsideHandler = () => {
    setIsActive(false);
  };

  useOnClickOutside(sideBarRef, clickOutsideHandler);

  const { theme, toggleTheme } = useThemeContext();

  return (
    <Overlay $active={isActive}>
      <Nav $active={isActive} ref={sideBarRef}>
        <Icon>
          <TfiClose
            onClick={() => {
              setIsActive((prev) => !prev);
            }}
          />
        </Icon>

        <NavContainer>
          <Link to="/home">Главная</Link>
          <Link to="/favorites">Мои Треки</Link>
          <Theme onClick={toggleTheme}>{theme === 'dark' ? <HiMoon /> : <HiSun />}</Theme>
        </NavContainer>
      </Nav>
    </Overlay>
  );
};
export default SideBar;
