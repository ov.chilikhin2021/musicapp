import { NavLink } from 'react-router-dom';
import { styled } from 'styled-components';

export const Overlay = styled.section<{ $active: boolean }>`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: ${(props) => (props.$active ? 1 : -1)};
  transition: background 0.3s linear;

  background: ${(props) => (props.$active ? props.theme.overlay : `transparent`)};
`;

export const Nav = styled.nav<{ $active: boolean }>`
  position: fixed;
  left: ${(props) => (props.$active ? `0` : '-1200px')};
  width: 300px;
  height: 100%;
  background-color: ${(props) => props.theme.sideBarBack};
  transition: left 0.5s ease;
`;

export const Icon = styled.section`
  cursor: pointer;
  top: 1rem;
  right: 1.5rem;
  position: absolute;
`;

export const NavContainer = styled.section`
  gap: 1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 5rem 0;
`;

export const Link = styled(NavLink)`
  color: ${(props) => props.theme.color};
  width: 100%;
  text-align: center;
  padding: 0.5rem 0;
  transition: background 0.5s ease;
  &:hover {
    background: #a3a3a3;
  }
  &.active {
    border-right: ${(props) => props.theme.color} 2px solid;
  }
`;

export const Theme = styled.div`
  font-size: 20px;
  cursor: pointer;
  width: 40px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid ${(props) => props.theme.color};
  border-radius: 50%;
  transition:
    0.3s ease background,
    0.3s ease color;

  &:hover {
    color: ${(props) => props.theme.backgroundColor};
    background: ${(props) => props.theme.color};
  }
`;
