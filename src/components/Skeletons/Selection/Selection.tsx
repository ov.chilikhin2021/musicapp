import ContentLoader from 'react-content-loader';

const SkeletonSelection = () => {
  return (
    <ContentLoader
      speed={0.5}
      width={250}
      height={151}
      viewBox="0 0 250 150"
      backgroundColor="#313131"
      foregroundColor="#ecebeb"
    >
      <path d="M 0 0.491 h 250 v 150 H 0 z" />
    </ContentLoader>
  );
};

export default SkeletonSelection;
