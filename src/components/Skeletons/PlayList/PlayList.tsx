import ContentLoader from 'react-content-loader';

const SkeletonItem = () => (
  <ContentLoader
    speed={0.5}
    width={1200}
    height={51}
    viewBox="0 0 1200 51"
    backgroundColor="#313131"
    foregroundColor="#ecebeb"
  >
    <rect x="0" y="0" rx="0" ry="0" width="51" height="51" />
    <rect x="66" y="19" rx="0" ry="0" width="310" height="19" />
    <rect x="396" y="19" rx="0" ry="0" width="300" height="19" />
    <rect x="716" y="19" rx="0" ry="0" width="310" height="19" />
    <rect x="1036" y="19" rx="0" ry="0" width="100" height="19" />
  </ContentLoader>
);

export default SkeletonItem;
