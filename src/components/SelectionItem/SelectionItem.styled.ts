import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const Container = styled(Link)<{ $bImg: string }>`
  width: 250px;
  height: 150px;
  position: relative;
  background: ${({ $bImg }) => `url(${$bImg});`};
  cursor: pointer;
  background-repeat: no-repeat;
  background-position: center;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  transition: transform 0.3s linear;
  &:hover {
    transform: scale(1.05);
  }
`;

export const Text = styled.span`
  font-size: 24px;
  line-height: 32px;
  letter-spacing: -0.12px;
`;
