import { FC } from 'react';
import { To } from 'react-router-dom';

import { Container, Text } from './SelectionItem.styled';

type SelectionItemProps = {
  to: To;
  text: string;
  img: string;
};

const SelectionItem: FC<SelectionItemProps> = ({ to, text, img }) => {
  return (
    <Container to={to} $bImg={img}>
      <Text>{text}</Text>
    </Container>
  );
};
export default SelectionItem;
