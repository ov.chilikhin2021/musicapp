import styled from 'styled-components';

export const VolumeSection = styled.section`
  display: flex;
  font-size: 20px;
  gap: 20px;
  align-items: center;
`;

export const Icon = styled.div``;

export const VolumeRange = styled.input`
  -webkit-appearance: none;
  appearance: none;

  width: 100%;
  cursor: pointer;
  outline: none;
  height: 2px;

  &::-webkit-slider-thumb {
    -webkit-appearance: none;
    width: 12px;
    height: 12px;
    border: 2px solid ${(props) => props.theme.volume_emp};
    border-radius: 50%;
    background: ${(props) => props.theme.volume_fill};
  }
`;
