import { ChangeEvent, FC, RefObject, useEffect, useState } from 'react';
import { TbVolume, TbVolume2, TbVolume3, TbVolumeOff } from 'react-icons/tb';
import { useTheme } from 'styled-components';

import { Icon, VolumeRange, VolumeSection } from './Right.styled';

type RightProps = {
  audio: RefObject<HTMLAudioElement>;
};

export const Right: FC<RightProps> = ({ audio }) => {
  const [volume, setVolume] = useState(60);

  const theme = useTheme();

  const [isMuted, setIsMuted] = useState(false);

  useEffect(() => {
    if (!audio.current) {
      return;
    }

    audio.current.volume = volume / 100;
    audio.current.muted = isMuted;
  }, [volume, audio, isMuted]);

  const VolumeIcon = () => {
    if (isMuted) {
      return <TbVolumeOff />;
    }
    if (volume === 0) {
      return <TbVolume3 />;
    } else if (volume < 30) {
      return <TbVolume2 />;
    } else {
      return <TbVolume />;
    }
  };

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setIsMuted(false);
    setVolume(Number(e.target.value));
  };

  return (
    <VolumeSection>
      <Icon
        onClick={() => {
          setIsMuted((prev) => !prev);
        }}
      >
        {VolumeIcon()}
      </Icon>

      <VolumeRange
        type="range"
        value={volume}
        style={{
          background: `linear-gradient(to right, ${theme.volume_fill} ${volume}%,  ${theme.volume_emp} ${volume}%)`,
        }}
        onChange={(e: ChangeEvent<HTMLInputElement>) => {
          onChange(e);
        }}
      />
    </VolumeSection>
  );
};
export default Right;
