import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { setIsPlaying } from '@/store/features/player/playerSlice';
import { RootState } from '@/store/store';

import Center from './Center/Center';
import Left from './Left/Left';
import { Bar, PlayerContainer } from './Player.styled';
import Progress from './Progress/Progress';
import Right from './Right/Right';

const Player = () => {
  const audioRef = useRef<HTMLAudioElement>(null);

  const currentSong = useSelector((state: RootState) => state.player.currentSong);
  const dispatch = useDispatch();
  const [duration, setDuration] = useState(0);
  const [timeProgress, setTimeProgress] = useState(0);

  const onLoad = () => {
    if (!audioRef.current) return;
    const seconds = Math.floor(audioRef.current.duration);
    setDuration(seconds);
  };

  useEffect(() => {
    if (!currentSong) return;
    if (!audioRef.current) return;
    audioRef.current.src = currentSong.track_file;
    audioRef.current.autoplay = true;
    dispatch(setIsPlaying(true));
  }, [currentSong]);

  return (
    <PlayerContainer>
      <audio ref={audioRef} preload="metadata" onLoadedMetadata={onLoad} />
      <Progress audio={audioRef} duration={duration} timeProgress={timeProgress} />
      <Bar>
        <Left audio={audioRef} setTimeProgress={setTimeProgress} />
        <Center />
        <Right audio={audioRef} />
      </Bar>
    </PlayerContainer>
  );
};
export default Player;
