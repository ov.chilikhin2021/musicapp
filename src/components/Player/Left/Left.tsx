import { Dispatch, FC, RefObject, useCallback, useEffect } from 'react';
import {
  TfiControlPause,
  TfiControlPlay,
  TfiControlShuffle,
  TfiControlSkipBackward,
  TfiControlSkipForward,
  TfiReload,
} from 'react-icons/tfi';
import { useDispatch, useSelector } from 'react-redux';

import {
  nextSong,
  prevSong,
  setIsLoop,
  setIsPlaying,
  setIsShuffle,
} from '@/store/features/player/playerSlice';
import { RootState } from '@/store/store';

import { Container, Icon } from './Left.styled';

type LeftProps = {
  audio: RefObject<HTMLAudioElement>;
  setTimeProgress: Dispatch<React.SetStateAction<number>>;
};

export const Left: FC<LeftProps> = ({ audio, setTimeProgress }) => {
  const dispatch = useDispatch();
  const isPlay = useSelector((state: RootState) => state.player.isPlaying);
  const isShuffle = useSelector((state: RootState) => state.player.isShuffle);
  const isRepeat = useSelector((state: RootState) => state.player.isLoop);

  const repeat = useCallback(() => {
    if (!audio.current) return;
    const currentTime = audio.current.currentTime;
    setTimeProgress(currentTime);

    requestAnimationFrame(repeat);
  }, [audio, setTimeProgress]);

  useEffect(() => {
    if (!audio.current) return;
    /* if (!playAnimationRef.current) return;*/
    if (isPlay) {
      audio.current.play();
    } else {
      audio.current.pause();
    }

    requestAnimationFrame(repeat);
  }, [isPlay, repeat]);

  useEffect(() => {
    if (!audio.current) return;

    audio.current.loop = isRepeat;
  }, [isRepeat]);

  useEffect(() => {
    if (!audio.current) return;

    audio.current.onended = onEnded;
  }, []);

  const onShuffle = () => {
    dispatch(setIsShuffle(!isShuffle));
  };

  const onRepeat = () => {
    dispatch(setIsLoop(!isRepeat));
  };

  const onPlay = () => {
    dispatch(setIsPlaying(!isPlay));
  };

  const onEnded = () => {
    dispatch(setIsPlaying(false));
    dispatch(nextSong());
    dispatch(setIsPlaying(true));
  };

  return (
    <Container>
      <Icon
        onClick={() => {
          dispatch(prevSong());
        }}
      >
        <TfiControlSkipBackward />
      </Icon>
      <Icon onClick={onPlay}>{isPlay ? <TfiControlPause /> : <TfiControlPlay />}</Icon>
      <Icon
        onClick={() => {
          dispatch(nextSong());
        }}
      >
        <TfiControlSkipForward />
      </Icon>
      <Icon onClick={onShuffle}>
        <TfiControlShuffle className={isShuffle ? 'icon active' : 'icon'} />
      </Icon>
      <Icon onClick={onRepeat}>
        <TfiReload className={isRepeat ? 'icon active' : 'icon'} />
      </Icon>
    </Container>
  );
};
export default Left;
