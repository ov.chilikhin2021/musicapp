import styled from 'styled-components';

export const Container = styled.section`
  display: flex;
  align-items: center;
  gap: 32px;
  color: #696969;
  font-size: 20px;
`;

export const Icon = styled.div`
  cursor: pointer;

  width: 40px;
  height: 40px;

  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;

  transition: background 0.3s linear;

  & > svg {
    fill: ${(props) => props.theme.icon};
    transition: fill 0.3s linear;
  }

  & > .icon {
    fill: ${(props) => props.theme.icon_shuffle};
  }

  & > .active {
    fill: ${(props) => props.theme.icon_active};
  }

  &:hover {
    background: #000;
    > svg {
      fill: ${(props) => props.theme.icon};
    }
    > .icon {
      fill: ${(props) => props.theme.icon_hover};
    }
  }
`;
