import { TbMusic } from 'react-icons/tb';
import { useSelector } from 'react-redux';

import { RootState } from '@/store/store';
import { Track } from '@/types/tracks.types';

import { Container, IconContainer, TextContainer } from './Center.styled';

const Center = () => {
  const currentSong = useSelector(
    (state: RootState) => state.player.currentSong,
  ) as Track;
  return (
    <Container>
      <IconContainer>
        <TbMusic />
      </IconContainer>
      <TextContainer>
        <p>{currentSong.name}</p>
        <p>{currentSong.author}</p>
      </TextContainer>
    </Container>
  );
};
export default Center;
