import styled from 'styled-components';

export const IconContainer = styled.div`
  font-size: 1.125rem;
  margin-right: 1rem;
  width: 51px;
  height: 51px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${(props) => props.theme.gray};
  background: ${(props) => props.theme.lightgray};
`;

export const Container = styled.div`
  display: flex;
  align-items: center;
`;

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`;
