import styled from 'styled-components';

export const PlayerContainer = styled.footer`
  color: ${(props) => props.theme.color};
  position: sticky;
  width: 100%;
  height: 80px;
  box-shadow: 0 0 75px #131313;
  background: ${(props) => props.theme.player};
  bottom: 0;
`;

export const Bar = styled.section`
  padding: 0 34px;
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
