import { ChangeEvent, FC, RefObject, useEffect, useState } from 'react';
import { useTheme } from 'styled-components';

import { ProgressBar } from './Progress.styled';

type ProgressProps = {
  audio: RefObject<HTMLAudioElement>;
  duration: number;
  timeProgress: number;
};

const Progress: FC<ProgressProps> = ({ audio, duration, timeProgress }) => {
  const [progress, setProgress] = useState(0);

  const theme = useTheme();

  useEffect(() => {
    setProgress((timeProgress / duration) * 100);
  }, [timeProgress]);

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (!audio.current) return;
    const value = Number(e.target.value);
    setProgress(value);
    audio.current.currentTime = (value * duration) / 100;
  };

  const value = (timeProgress / duration) * 100;
  return (
    <ProgressBar
      type="range"
      value={value}
      min={0}
      max={100}
      style={{
        background: `linear-gradient(to right, #B672FF ${progress}%, ${theme.progress} ${progress}%)`,
      }}
      onChange={(e: ChangeEvent<HTMLInputElement>) => {
        onChange(e);
      }}
    ></ProgressBar>
  );
};
export default Progress;
