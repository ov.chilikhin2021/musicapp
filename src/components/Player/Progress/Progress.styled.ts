import styled from 'styled-components';

export const ProgressBar = styled.input`
  cursor: pointer;
  overflow: hidden;
  width: 100%;
  height: 5px;
  position: absolute;
  appearance: none;
  top: 0;
  left: 0;
  transition: height 0.3s;
  &:hover {
    height: 10px;
  }

  &::-webkit-slider-thumb {
    appearance: none;
    width: 0;
    height: 0;
  }
`;
