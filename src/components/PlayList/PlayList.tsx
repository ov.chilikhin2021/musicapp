import { FC } from 'react';
import { TbClockHour9 } from 'react-icons/tb';
import { useDispatch } from 'react-redux';

import { setCurrentSong, setTracks } from '@/store/features/player/playerSlice';
import { Track } from '@/types/tracks.types';

import PlayListItem from '../PlayListItem/PlayListItem';
import SkeletonItem from '../Skeletons/PlayList/PlayList';
import {
  PlayListColumn,
  PlayListContainer,
  PlayListHeader,
  PlayListMain,
} from './PlayList.styled';

type PlayListProps = {
  data?: Track[] | Omit<Track, 'stared_user'>[];
  isLoading?: boolean;
};
const PlayList: FC<PlayListProps> = ({ isLoading, data }) => {
  const SkeletonArray = new Array<Element | number>(8).fill(0);

  const dispatch = useDispatch();

  const onClick = (
    data: Track[] | Omit<Track, 'stared_user'>[],
    item: Track | Omit<Track, 'stared_user'>,
  ) => {
    dispatch(setTracks(data));
    dispatch(setCurrentSong(item));
  };

  return (
    <PlayListContainer>
      <PlayListHeader>
        <PlayListColumn>Трек</PlayListColumn>
        <PlayListColumn>Исполнитель</PlayListColumn>
        <PlayListColumn>Альбом</PlayListColumn>
        <PlayListColumn>
          <TbClockHour9 />
        </PlayListColumn>
      </PlayListHeader>

      <PlayListMain>
        {isLoading ? (
          <>
            {SkeletonArray.map((_, id) => (
              <SkeletonItem key={id} />
            ))}
          </>
        ) : (
          <>
            {data?.map((item, id) => (
              <PlayListItem
                onClick={() => {
                  onClick(data, item);
                }}
                key={id}
                data={item}
              />
            ))}
          </>
        )}
      </PlayListMain>
    </PlayListContainer>
  );
};
export default PlayList;
