import { styled } from 'styled-components';

export const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  gap: 10px;
`;

export const Item = styled.div`
  border: 1px solid ${(props) => props.theme.color};
  padding: 6px 20px;
  border-radius: 60px;
  display: flex;
  justify-content: center;
  text-transform: lowercase;
  cursor: pointer;
  transition:
    color 0.3s linear,
    border 0.3s linear;

  &.active:hover,
  &:hover {
    color: #d9b6ff;
    border-color: #d9b6ff;
  }

  &.active {
    color: #ad61ff;
    border-color: #ad61ff;
  }
`;
