import { FC } from 'react';

import { Author, Genre, Year } from '@/types/tracks.types';

import DropDown from '../DropDown/DropDown';
import { Container, Item } from './FilterItem.styled';

type FilterItemProps = {
  name: string;
  type: string;
  data: Author[] | Year[] | Genre[];
  onClick: () => void;
  isActive: boolean;
};

const FilterItem: FC<FilterItemProps> = ({ name, type, isActive, onClick, data }) => {
  return (
    <Container>
      <Item
        className={isActive ? 'active' : ''}
        onClick={() => {
          onClick();
        }}
      >
        {name}
      </Item>
      <DropDown type={type} data={data} className={isActive ? 'active' : ''} />
    </Container>
  );
};
export default FilterItem;
