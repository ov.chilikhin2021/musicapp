import styled from 'styled-components';

export const Selection = styled.aside`
  padding: 2rem 0;
  display: flex;
  gap: 1.875rem;
  flex-direction: column;
`;
