import { FC } from 'react';

import img1 from '@/assets/images/playlist1.png';
import img2 from '@/assets/images/playlist2.png';
import img3 from '@/assets/images/playlist3.png';

import SelectionItem from '../SelectionItem/SelectionItem';
import SkeletonSelection from '../Skeletons/Selection/Selection';
import { Selection } from './Selection.styled';

type SelectionProps = {
  isLoading?: boolean;
};
const Selections: FC<SelectionProps> = ({ isLoading }) => {
  return (
    <Selection>
      {isLoading ? (
        <>
          <SkeletonSelection />
          <SkeletonSelection />
          <SkeletonSelection />
        </>
      ) : (
        <>
          <SelectionItem img={img1} text="Плейлист дня" to="/category/1" />
          <SelectionItem img={img2} text="100 танцевальных хитов" to="/category/2" />
          <SelectionItem img={img3} text="Инди-заряд" to="/category/3" />
        </>
      )}
    </Selection>
  );
};
export default Selections;
