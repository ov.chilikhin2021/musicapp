import { FC } from 'react';

import { Author, Genre, Year } from '@/types/tracks.types';

import DropDownItem from '../DropDownItem/DropDownItem';
import { Container, DropDownContainer } from './DropDown.styled';

type DropDownType = {
  type: string;
  className: string;
  data: Author[] | Year[] | Genre[];
};
const DropDown: FC<DropDownType> = ({ type, className, data }) => {
  return (
    <Container className={className}>
      <DropDownContainer>
        {data.map((item, id) => (
          <DropDownItem type={type} key={id} item={item} />
        ))}
      </DropDownContainer>
    </Container>
  );
};
export default DropDown;
