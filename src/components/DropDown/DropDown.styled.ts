import styled from 'styled-components';

export const Container = styled.div`
  position: absolute;
  z-index: -1;
  top: 40px;
  width: 248px;
  height: 305px;
  background: ${(props) => props.theme.lightgray};
  border-radius: 12px;
  padding: 34px;

  &.active {
    z-index: 1;
  }
`;

export const DropDownContainer = styled.ul`
  display: flex;
  flex-direction: column;
  gap: 25px;
  height: 232px;
  overflow-y: auto;
  list-style: none;

  &::-webkit-scrollbar {
    appearance: none;
    width: 4px;
  }

  &::-webkit-scrollbar-track {
    width: 4px;
    border-radius: 10px;
    background: ${(props) => props.theme.backgroundColor};
  }
  &::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background: ${(props) => props.theme.color};
  }
`;
