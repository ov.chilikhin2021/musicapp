import { years } from 'data/Filters';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import { useTypedSelector } from '@/hooks/useTypedSelector';
import { setAuthors, setGenres } from '@/store/features/filter/filterSlice';

import FilterItem from '../FilterItem/FilterItem';
import { Container, Filter, Text } from './Filters.styled';

const Filters = () => {
  const [isOpenFilter, setIsOpenFilter] = useState<string>();

  const dispatch = useDispatch();
  const { tracks, authors, genres } = useTypedSelector((state) => state.filter);

  useEffect(() => {
    dispatch(setAuthors());
    dispatch(setGenres());
  }, [tracks]);

  return (
    <Filter>
      <Text>Искать по: </Text>
      <Container>
        <FilterItem
          type="author"
          data={authors}
          name="Исполнителю"
          isActive={isOpenFilter === 'author'}
          onClick={() => {
            setIsOpenFilter((prev) => (prev === 'author' ? '' : 'author'));
          }}
        />
        <FilterItem
          type="year"
          data={years}
          name="Году выпуска"
          isActive={isOpenFilter === 'year'}
          onClick={() => {
            setIsOpenFilter((prev) => (prev === 'year' ? '' : 'year'));
          }}
        />
        <FilterItem
          type="genre"
          data={genres}
          name="Жанру"
          isActive={isOpenFilter === 'genre'}
          onClick={() => {
            setIsOpenFilter((prev) => (prev === 'genre' ? '' : 'genre'));
          }}
        />
      </Container>
    </Filter>
  );
};
export default Filters;
