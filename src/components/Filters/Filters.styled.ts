import { styled } from 'styled-components';

export const Filter = styled.section`
  display: flex;
  gap: 15px;
  align-items: center;
`;

export const Text = styled.span``;

export const Container = styled.div`
  display: flex;
  gap: 10px;
`;
