import { Year } from '@/types/tracks.types';

export const years: Year[] = [
  {
    name: 'По возрастанию',
    value: 'true',
  },
  {
    name: 'По убыванию',
    value: 'false',
  },
];
