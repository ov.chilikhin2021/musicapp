import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';

import { authApi } from './features/auth/authApi';
import filterSlice from './features/filter/filterSlice';
import playerReducer from './features/player/playerSlice';
import { trackApi } from './features/track/trackApi';
import userReducer from './features/user/userSlice';

export const store = configureStore({
  reducer: {
    [authApi.reducerPath]: authApi.reducer,
    [trackApi.reducerPath]: trackApi.reducer,
    player: playerReducer,
    user: userReducer,
    filter: filterSlice,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(authApi.middleware).concat(trackApi.middleware),
  devTools: true,
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
setupListeners(store.dispatch);
