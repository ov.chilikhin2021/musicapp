import { createSlice, current, PayloadAction } from '@reduxjs/toolkit';

import { Player } from '@/types/player.types';
import { Track } from '@/types/tracks.types';

const initialState: Player = {
  currentSong: undefined,
  tracks: [],
  isShuffle: false,
  isLoop: false,
  isPlaying: false,
};

const playerSlice = createSlice({
  name: 'playerSlice',
  initialState,
  reducers: {
    setCurrentSong(state, action: PayloadAction<Track>) {
      return {
        ...state,
        currentSong: action.payload,
      };
    },

    setTracks(state, action: PayloadAction<Track[]>) {
      return {
        ...state,
        tracks: action.payload,
      };
    },

    setIsPlaying(state, action: PayloadAction<boolean>) {
      return {
        ...state,
        isPlaying: action.payload,
      };
    },

    setIsShuffle(state, action: PayloadAction<boolean>) {
      return {
        ...state,
        isShuffle: action.payload,
      };
    },

    setIsLoop(state, action: PayloadAction<boolean>) {
      return {
        ...state,
        isLoop: action.payload,
      };
    },

    nextSong(state) {
      const isShuffle = state.isShuffle;
      const currentSong = current(state.currentSong) as Track;
      const tracks = current(state.tracks) as Track[];

      if (!currentSong) return;

      if (isShuffle) {
        const nextTrack = Math.floor(Math.random() * (state.tracks.length - 1));
        const track = tracks[nextTrack] as Track;
        state.currentSong = track;
        return;
      }

      const index = tracks.indexOf(currentSong) + 1;

      if (index > tracks.length - 1) {
        state.currentSong = tracks[0];
      } else {
        state.currentSong = tracks[index];
      }
    },

    prevSong(state) {
      const isShuffle = state.isShuffle;
      const currentSong = current(state.currentSong) as Track;
      const tracks = current(state.tracks) as Track[];

      if (!currentSong) return;

      if (isShuffle) {
        const nextTrack = Math.floor(Math.random() * (state.tracks.length - 1));
        const track = tracks[nextTrack] as Track;
        state.currentSong = track;
        return;
      }

      const index = tracks.indexOf(currentSong) - 1;

      if (index < 0) {
        state.currentSong = tracks[tracks.length - 1];
      } else {
        state.currentSong = tracks[index];
      }
    },
  },
});

export const {
  setCurrentSong,
  setTracks,
  setIsPlaying,
  setIsLoop,
  setIsShuffle,
  nextSong,
  prevSong,
} = playerSlice.actions;

export default playerSlice.reducer;
