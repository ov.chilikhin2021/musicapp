import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import { IRegister, Login, Token, User } from '@/types/user.types';

const baseQuery = fetchBaseQuery({
  baseUrl: 'https://painassasin.online/user',
});

export const authApi = createApi({
  reducerPath: 'authApi',
  baseQuery: baseQuery,
  endpoints: (builder) => ({
    loginUser: builder.mutation<User, Login>({
      query: (body) => {
        return { url: '/login/', method: 'POST', body };
      },
    }),
    registerUser: builder.mutation({
      query: (body: IRegister) => {
        return { url: '/signup/', method: 'POST', body };
      },
    }),
    tokenUser: builder.mutation<Token, Login>({
      query: (body) => {
        return { url: '/token/', method: 'POST', body };
      },
    }),
  }),
});

export const { useLoginUserMutation, useRegisterUserMutation, useTokenUserMutation } =
  authApi;
