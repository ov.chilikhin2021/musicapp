import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Refresh, Token, User } from '@/types/user.types';

const initialState = {
  user: JSON.parse(localStorage.getItem('user') || '{}') as User,
  access: localStorage.getItem('access') || '',
  refresh: localStorage.getItem('refresh') || '',
};

const userSlice = createSlice({
  name: 'userSlice',
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<User>) => {
      localStorage.setItem('user', JSON.stringify(action.payload));
      return {
        ...state,
        user: action.payload,
      };
    },
    setTokens: (state, { payload }: PayloadAction<Token>) => {
      localStorage.setItem('access', payload.access);
      localStorage.setItem('refresh', payload.refresh);

      return {
        ...state,
        access: payload.access,
        refresh: payload.refresh,
      };
    },
    setAccess: (state, { payload }: PayloadAction<Refresh>) => {
      return {
        ...state,
        access: payload.access,
      };
    },

    logOut: (state) => {
      localStorage.removeItem('user');
      localStorage.removeItem('access');
      localStorage.removeItem('refresh');

      return {
        ...state,
        access: '',
        user: {} as User,
        refresh: '',
      };
    },
  },
});

export const { setUser, setTokens, setAccess, logOut } = userSlice.actions;

export default userSlice.reducer;
