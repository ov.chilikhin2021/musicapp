import {
  BaseQueryFn,
  createApi,
  FetchArgs,
  fetchBaseQuery,
  FetchBaseQueryError,
} from '@reduxjs/toolkit/dist/query/react';

import { RootState } from '@/store/store';
import { Selection, Track } from '@/types/tracks.types';
import { Refresh } from '@/types/user.types';

import { logOut, setAccess } from '../user/userSlice';

const baseQuery = fetchBaseQuery({
  baseUrl: 'https://painassasin.online/',
  prepareHeaders: (headers, { getState }) => {
    const token = (getState() as RootState).user.access;

    // If we have a token set in state, let's assume that we should be passing it.
    if (token) {
      headers.set('authorization', `Bearer ${token}`);
    }

    return headers;
  },
});

const baseQueryWithReauth: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api, extraOptions) => {
  let result = await baseQuery(args, api, extraOptions);
  if (result.error && result.error.status === 401) {
    // try to get a new token
    const refreshResult = await baseQuery(
      {
        url: 'user/token/refresh/',
        method: 'POST',
        body: { refresh: localStorage.getItem('refresh') },
        responseHandler: (response) => response.json(),
      },
      api,
      extraOptions,
    );
    if (refreshResult.data) {
      // store the new token
      api.dispatch(setAccess(refreshResult.data as Refresh));
      // retry the initial query
      result = await baseQuery(args, api, extraOptions);
    } else {
      api.dispatch(logOut());
    }
  }
  return result;
};

export const trackApi = createApi({
  reducerPath: 'trackApi',
  baseQuery: baseQueryWithReauth,
  tagTypes: ['Track'],
  endpoints: (builder) => ({
    getAllTracks: builder.query<Track[], void>({
      query: () => ({
        url: 'catalog/track/all/',
      }),
      providesTags: ['Track'],
    }),
    getSelectionById: builder.query<Selection, number>({
      query: (id) => ({
        url: `catalog/selection/${id}/`,
      }),
      providesTags: ['Track'],
    }),
    getFavoriteTracks: builder.query<Omit<Track, 'stared_user'>[], void>({
      query: () => ({
        url: 'catalog/track/favorite/all/',
      }),
      providesTags: ['Track'],
    }),

    addFavoriteTrack: builder.mutation<void, number>({
      query: (id) => ({
        url: `catalog/track/${id}/favorite/`,
        method: 'POST',
      }),
      invalidatesTags: ['Track'],
    }),

    removeFavoriteTrack: builder.mutation<void, number>({
      query: (id) => ({
        url: `catalog/track/${id}/favorite/`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Track'],
    }),
  }),
});

export const {
  useGetAllTracksQuery,
  useGetSelectionByIdQuery,
  useGetFavoriteTracksQuery,
  useAddFavoriteTrackMutation,
  useRemoveFavoriteTrackMutation,
} = trackApi;
