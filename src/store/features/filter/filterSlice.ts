import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Author, Genre, Track, Year } from '@/types/tracks.types';

const initialState = {
  tracks: [] as Track[],
  filterTracks: [] as Track[],
  search: '',
  authors: [] as Author[],
  genres: [] as Genre[],
  selectedGenres: [] as Genre[],
  selectedAuthors: [] as Author[],
  sortedByAsc: '',
};

const filterSlice = createSlice({
  name: 'filterSlice',
  initialState,
  reducers: {
    setTracks(state, action: PayloadAction<Track[]>) {
      return {
        ...state,
        tracks: action.payload,
      };
    },

    setSearch(state, action: PayloadAction<string>) {
      return {
        ...state,
        search: action.payload,
      };
    },

    setAuthors(state) {
      const authors = new Array<Author>();

      state.tracks.map((track) => {
        const author: Author = { name: track.author, value: track.author.toLowerCase() };
        authors.push(author);
      });

      const unique = authors
        .filter(
          (obj, index) => authors.findIndex((item) => item.name === obj.name) === index,
        )
        .sort((a, b) => (a.name > b.name ? 1 : -1));

      return {
        ...state,
        authors: [...unique],
      };
    },

    setGenres(state) {
      const genres = new Array<Genre>();

      state.tracks.map((track) => {
        const genre: Genre = { name: track.genre, value: track.genre.toLowerCase() };
        genres.push(genre);
      });

      const unique = genres
        .filter(
          (obj, index) => genres.findIndex((item) => item.name === obj.name) === index,
        )
        .sort((a, b) => (a.name > b.name ? 1 : -1));

      return {
        ...state,
        genres: [...unique],
      };
    },

    setSelectedGenres(state, action: PayloadAction<Genre>) {
      const index = state.selectedGenres.findIndex(
        (genre) => genre.name === action.payload.name,
      );

      index === -1
        ? state.selectedGenres.push(action.payload)
        : state.selectedGenres.splice(index, 1);
    },

    setSelectedAuthors(state, action: PayloadAction<Author>) {
      const index = state.selectedAuthors.findIndex(
        (author) => author.name === action.payload.name,
      );

      index === -1
        ? state.selectedAuthors.push(action.payload)
        : state.selectedAuthors.splice(index, 1);
    },

    setSorted(state, action: PayloadAction<Year>) {
      return {
        ...state,
        sortedByAsc: action.payload.value,
      };
    },

    setFilterData(state) {
      const sortedByAsc = state.sortedByAsc;

      const selectedGenres = state.selectedGenres;
      const selectedAuthors = state.selectedAuthors;

      const ListTrackByGenres: Track[] = [];
      const ListTrackByAuthors: Track[] = [];

      sortedByAsc
        ? sortedByAsc === 'false'
          ? (state.filterTracks = state.tracks.sort((a, b) => {
              const date_a = new Date(a.release_date ? a.release_date : '').getTime();
              const date_b = new Date(b.release_date ? b.release_date : '').getTime();

              return date_b - date_a;
            }))
          : (state.filterTracks = state.tracks.sort((a, b) => {
              const date_a = new Date(a.release_date ? a.release_date : '').getTime();
              const date_b = new Date(b.release_date ? b.release_date : '').getTime();

              return date_a - date_b;
            }))
        : (state.filterTracks = state.tracks);

      selectedGenres.map((item) => {
        ListTrackByAuthors.length !== 0
          ? state.filterTracks
              .filter((track) => track.genre === item.name)
              .map((track) => ListTrackByGenres.push(track))
          : state.tracks
              .filter((track) => track.genre === item.name)
              .map((track) => ListTrackByGenres.push(track));
        state.filterTracks = ListTrackByGenres;
      });

      selectedAuthors.map((item) => {
        console.log(ListTrackByGenres);
        ListTrackByGenres.length !== 0
          ? state.filterTracks
              .filter((track) => track.author === item.name)
              .map((track) => ListTrackByAuthors.push(track))
          : state.tracks
              .filter((track) => track.author === item.name)
              .map((track) => ListTrackByAuthors.push(track));

        console.log(ListTrackByAuthors);
        state.filterTracks = ListTrackByAuthors;
      });
    },
  },
});
export const {
  setTracks,
  setAuthors,
  setSearch,
  setGenres,
  setSelectedGenres,
  setSelectedAuthors,
  setSorted,
  setFilterData,
} = filterSlice.actions;
export default filterSlice.reducer;
