import { Theme } from '@/types/themes.types';

export const DarkTheme: Theme = {
  backgroundColor: '#000',
  color: '#fff',
  gray: '#4e4e4e',
  lightgray: '#313131',
  border: '#4e4e4e',
  sideBarBack: '#1C1C1C',
  overlay: 'rgba(255, 255, 255, 0.5)',
  player: `rgba(28, 28, 28)`,
  progress: '#2E2E2E',
  volume_fill: '#fff',
  volume_emp: '#797979',
  icon: '#D9D9D9',
  icon_shuffle: '#696969',
  icon_active: '#d9d9d9',
  icon_hover: '#acacac',
};

export default DarkTheme;
