import { Theme } from '@/types/themes.types';

const LightTheme: Theme = {
  backgroundColor: '#fff',
  color: '#000',
  gray: '#B1B1B1',
  lightgray: '#F6F4F4',
  border: '#D9D9D9',
  sideBarBack: '#F6F5F3',
  overlay: 'rgba(0, 0, 0, 0.5)',
  player: `#fff`,
  progress: '#D9D9D9',
  volume_fill: '#AD61FF',
  volume_emp: '#B1B1B1',
  icon: '#B1B1B1',
  icon_shuffle: '#b1b1b1',
  icon_active: '#000',
  icon_hover: '#707070',
};

export default LightTheme;
