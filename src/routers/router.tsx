import { useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import { useAuth } from '@/hooks/useAuth';

import { Empty, Private } from '../layouts';
import { Collection, Favorites, Home, Login, NotFound, Register } from '../pages';

export const Router = () => {
  const [isAuth, setIsAuth] = useState(false);
  const { user } = useAuth();

  useEffect(() => {
    user.id ? setIsAuth(true) : setIsAuth(false);
  }, [user]);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Empty isAuth={isAuth} />}>
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="*" element={<NotFound />} />
        </Route>
        <Route path="/" element={<Private isAuth={isAuth} />}>
          <Route path="/home" element={<Home />} />
          <Route path="/favorites" element={<Favorites />} />
          <Route path="/category/:id" element={<Collection />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};
