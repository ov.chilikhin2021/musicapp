import { createContext, FC, PropsWithChildren, useContext, useState } from 'react';
import { ThemeProvider } from 'styled-components';

import { DarkTheme, LightTheme } from '../themes';

export const Theme = createContext({
  theme: '',
  toggleTheme: () => {},
});

export const useThemeContext = () => {
  const theme = useContext(Theme);

  return theme;
};

export const ThemeContext: FC<PropsWithChildren> = ({ children }) => {
  const [currentTheme, setCurrentTheme] = useState('dark');

  const toggleTheme = () => {
    if (currentTheme !== 'dark') {
      setCurrentTheme('dark');
      return;
    }

    setCurrentTheme('light');
  };
  return (
    <Theme.Provider value={{ theme: currentTheme, toggleTheme }}>
      <ThemeProvider theme={currentTheme === 'light' ? LightTheme : DarkTheme}>
        {children}
      </ThemeProvider>
    </Theme.Provider>
  );
};
