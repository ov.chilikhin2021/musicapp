export type Theme = {
  backgroundColor: string;
  color: string;
  gray: string;
  lightgray: string;
  border: string;
  sideBarBack: string;
  overlay: string;
  player: string;
  progress: string;
  volume_fill: string;
  volume_emp: string;
  icon: string;
  icon_shuffle: string;
  icon_active: string;
  icon_hover: string;
};
