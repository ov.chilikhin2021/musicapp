import { Track } from './tracks.types';

export type Player = {
  currentSong?: Track;
  tracks: Track[];
  isLoop: boolean;
  isShuffle: boolean;
  isPlaying: boolean;
};
