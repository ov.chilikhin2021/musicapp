export interface User {
  email: string;
  first_name: string;
  id: number;
  last_name: string;
  username: string;
}

export interface Token {
  access: string;
  refresh: string;
}

export interface Refresh {
  access: string;
}

export interface Login {
  email: string;
  password: string;
}

export interface IRegister {
  username: string;
  email: string;
  password: string;
  passwordConfirm: string;
}

export interface RefreshInput {
  refresh: string;
}
