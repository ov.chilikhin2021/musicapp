import { User } from './user.types';

export type Track = {
  album: string;
  author: string;
  duration_in_seconds: number;
  genre: string;
  id: number;
  logo?: string;
  name: string;
  release_date?: Date;
  track_file: string;
  stared_user?: User[];
};

export type Selection = {
  id: number;
  items: Track[];
  owner: string;
  name: string;
};

export type Author = {
  name: string;
  value: string;
};

export type Genre = {
  name: string;
  value: string;
};

export type Year = {
  name: string;
  value: 'false' | 'true';
};
